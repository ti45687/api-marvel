import { Router } from "express";
import { crearusuarios,login } from "../Controllers/AuthController.js";
const rutas = Router()

rutas.post('/api/usuarios',crearusuarios)
rutas.post('/api/login',login)
export default rutas